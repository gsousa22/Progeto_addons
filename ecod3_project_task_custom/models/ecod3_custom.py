# -*- coding: utf-8 -*-

from openerp import models, fields, api

class ECOD3_Ecod3Custom(models.Model):
    _inherit = 'project.task'
    
    # Campos Simples
    ecod3_active_01 = fields.Boolean(string = 'Ativo ?')

    # Campos Many2one)0
    ecod3_partner_filter_01 = fields.Many2one('project.task', u'Gerente:',  domain="[('ecod3_partner_filter_01','=', 'res.partner')]")