# -*- coding: utf-8 -*-
# Copyright 2016, 2017 Openworx
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "ECOD3 - ecod3_project_task_custom",
    "summary": "Exemplos de campos.",
    "version": "11.0.1.0.3",
    "category": "MRP",
    "website": "https://ecod3.com",
    "description": """
		Exemplos de campos do Odoo.
    """,
    'images':[
	],
    "author": "ECOD3",
    "license": "LGPL-3",
    "installable": True,
    "depends": [
        'contacts', 'project',
    ],
    "data": [
        'views/ecod3_custom.xml',
    ],
}